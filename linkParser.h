#pragma once

#include <libxml/HTMLparser.h>
#include "HyperLink.h"

/**
 * @brief Downloads the page pointed at by the provided HyperLink's href
 * and stores the result in the HyperLink's webPage
 * @param hyperLink the Hyperlink with the href to download
 * @return true if a page could be downloaded
 */
bool downloadWebPage(HyperLink & hyperLink);

/**
 * @brief Searches the xmlNode for href tags and adds them to the children vector
 * can be called recursively. Add a tuple of href and description to the links vector
 * @param a_node the xmlNode to find links in
 * @param links the output parameter to add the found links to
 * @return The number of links added
 */
int collectLinks(xmlNode *a_node, std::vector<std::tuple<std::wstring, std::wstring>> & links);

/**
 * @brief parses the webPage for links and adds them to the children vector.
 * @return The number of links added
 */
std::vector<std::tuple<std::wstring, std::wstring>> parseLinks( HyperLink & hyperLink);

/**
 * @brief Download the page indicated in the Hyperlink and parses the the
 * child links out of them.
 * @param hyperLink The Hyperlink with the href to download
 */
void populateChildren(HyperLink * hyperLink);
