#include <gmock/gmock.h>
using namespace ::testing;

#include "stringConversion.h"

TEST(stringConversion, wstringToStringUmlauts)
{
    std::string s = wstringToString( L"Bienvennue à Zürich");
    ASSERT_EQ("Bienvennue à Zürich", s);
}

TEST(stringConversion, stringToWstringUmlauts)
{
    std::wstring s = stringToWstring( "Bienvennue à Zürich");
    ASSERT_EQ(L"Bienvennue à Zürich", s);
}

TEST(stringConversion, xmlCharToWideStringNull)
{
    const xmlChar* c = nullptr;
    std::wstring s = xmlCharToWideString( c );
    ASSERT_EQ(L"Null pointer passed to xmlCharToWideString", s);
}

TEST(stringConversion, xmlCharToWideStringNormal)
{
    const xmlChar* c = reinterpret_cast<const unsigned char *>("test string");
    std::wstring s = xmlCharToWideString( c );
    ASSERT_EQ(L"test string", s);
}

TEST(stringConversion, xmlCharToWideStringFailure)
{
    std::string str = u8"z\u00df\u6c34\U0001f34c";
    unsigned char data[50];
    std::copy(str.begin(), str.end(), data);
    data[0] = 0xFF;
    std::wstring s = xmlCharToWideString( data );
    ASSERT_EQ(L"wstring_convert failed", s);
}
