# Copyright 2013-2017, Corvusoft Ltd, All Rights Reserved.

# Found at https://github.com/Corvusoft/restbed/tree/master/cmake/modules
# Licenced under GNU Affero General Public License
# Note that I only cloned the 10 line Findasio.cmake file which is a helper
# to build the software and have not included any parts of the restbed software
# itself. Richard Eigenmann 2017

find_path( asio_INCLUDE asio.hpp HINTS "${PROJECT_SOURCE_DIR}/dependency/asio/asio/include" "/usr/include" "/usr/local/include" "/opt/local/include" )

if ( asio_INCLUDE )
    set( ASIO_FOUND TRUE )
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DASIO_STANDALONE=YES" )

    message( STATUS "${Green}Found ASIO include at: ${asio_INCLUDE}${Reset}" )
else ( )
    message( FATAL_ERROR "${Red}Failed to locate ASIO dependency.${Reset}" )
endif ( )
