hunter_config(ZLIB VERSION ${HUNTER_ZLIB_VERSION} CMAKE_ARGS BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS})
hunter_config(CURL VERSION ${HUNTER_CURL_VERSION} CMAKE_ARGS BUILD_SHARED_LIBS=${BUILD_SHARED_LIBS})
hunter_config(CURL VERSION ${HUNTER_CURL_VERSION} CMAKE_USE_OPENSSL OFF)
