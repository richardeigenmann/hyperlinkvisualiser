# The intent of this Dockerfile is to set up a C++17 compiler and CMake
# environment with required libraries to compile the sources in the
# supplied sourcedir directory

# To create the container:
# docker build -t opensuse/cpp17compiler .

# To run the container:
# docker run -it --hostname cpp17compiler -v <host-sourcedir>:/sourcedir -p 8080:8080 --rm opensuse/cpp17compiler <cmd>

# To push to dockerhub
# see https://ropenscilabs.github.io/r-docker-tutorial/04-Dockerhub.html
# docker images
# docker tag <<number>> richardeigenmann/opensusecpp17
# docker push richardeigenmann/opensusecpp17

FROM opensuse:tumbleweed

# Adds the repos with clang5 and the general environment
RUN  zypper addrepo -f --no-gpgcheck http://download.opensuse.org/repositories/devel:/tools:/compiler/openSUSE_Factory/devel:tools:compiler.repo  \
  && zypper addrepo -f --no-gpgcheck http://download.opensuse.org/repositories/devel:/libraries:/c_c++/openSUSE_Factory/devel:libraries:c_c++.repo \
  && zypper install --no-confirm \
    git\
    clang5\
    gcc-c++ \
    cmake \
    vim \
    figlet \
    gcovr \
    lcov \
    which \
    doxygen \
    graphviz \
    curl \
    libcurl4-7.59.0-231.2.x86_64

# Install the packages that you require for headers or linked libraries
#RUN zypper install --no-confirm --force  \
#    libcurl-devel \
#    libcurl4-7.58.0-227.5.x86_64  \
#    libxml2-devel \
#    asio-devel \
#    libopenssl-devel 

VOLUME /sourcedir

# Build a dummy project to get Hunter to download and compile the libraries
RUN mkdir -p /warmup/build-gcc /warmup/build-clang
COPY CMakeLists.txt /warmup
COPY HunterGate.cmake /warmup
RUN cd /warmup/build-clang && cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++ .. && cmake --build . -- -j$(($(nproc) + 1))
RUN cd /warmup/build-gcc && cmake -DCMAKE_CXX_COMPILER=/usr/bin/g++ .. && cmake --build . -- -j$(($(nproc) + 1))

COPY welcome.sh /etc/profile.d/
RUN chmod u+x /etc/profile.d/welcome.sh

#ENTRYPOINT ["./welcome.sh"]
#ENTRYPOINT ["bash"]
