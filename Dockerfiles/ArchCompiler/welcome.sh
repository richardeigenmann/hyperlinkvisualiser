#!/bin/bash
cd /sourcedir

ppwd ()
{
    local dir;
    local -i width;
    test -n "$TS1" || return;
    dir="$(dirs +0)";
    let width=${#dir}-18;
    test ${#dir} -le 18 || dir="...${dir#$(printf "%.*s" $width "$dir")}";
    if test ${#TS1} -gt 17; then
        printf "$TS1" "$USER" "$HOST" "$dir" "$HOST";
    else
        printf "$TS1" "$USER" "$HOST" "$dir";
    fi
}

export PS1="\[$(ppwd)\]\u@\h:\w>"
figlet C++17
exec bash
