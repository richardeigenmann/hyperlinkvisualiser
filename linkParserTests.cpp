#include <gmock/gmock.h>
using namespace ::testing;

#include "HyperLink.h"
#include "linkParser.h"

TEST(linkParser, downloadWebPage) {
    HyperLink root { L"https://www.google.com", L"Google"};
    ASSERT_THAT(0, root.webPage.size());

    downloadWebPage(root);
    ASSERT_LT(100, root.webPage.size());
    ASSERT_EQ(L"Google", root.description);
}

TEST(linkParser, downloadInexistantWebPage) {
    HyperLink root { L"http://localhost:65000", L"Nothing"};
    ASSERT_THAT(0, root.webPage.size());

    downloadWebPage(root);
    ASSERT_THAT(0, root.webPage.size());
    ASSERT_EQ(L"Couldn't connect to server", root.description);
}

TEST(linkParser, parsLinks) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:1234", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    ASSERT_EQ( L"https://www.google.com", std::get<0>(links.at(0)) );
    ASSERT_EQ( L"Google", std::get<1>(links.at(0)) );
    ASSERT_EQ( L"https://www.ibm.com", std::get<0>(links.at(1)) );
    ASSERT_EQ( L"Ibm", std::get<1>(links.at(1)) );
    ASSERT_EQ( L"https://www.opensuse.org", std::get<0>(links.at(2)) );
    ASSERT_EQ( L"OpenSuse", std::get<1>(links.at(2)) );
}

TEST(linkParser, populateChildren) {
    HyperLink root { L"https://richardeigenmann.github.io/simplepage/links.htm", L"Static page"};
    ASSERT_THAT(0, root.children.size());
    populateChildren(&root);
    ASSERT_THAT(289, root.webPage.size());
    ASSERT_THAT(3, root.children.size());
}

TEST(linkParser, collectLinks) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

  _xmlDoc *htmlDocument = htmlReadDoc((const xmlChar *)htmldoc.c_str(), NULL, NULL,
                HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING);

  std::vector<std::tuple<std::wstring, std::wstring>> foundLinks {};
  ASSERT_TRUE (htmlDocument) << "The test should have been able to parse the html document";

  xmlNodePtr root_element = (xmlNodePtr)htmlDocument;
  collectLinks(root_element, foundLinks);
  ASSERT_THAT(3, foundLinks.size());
  ASSERT_EQ( L"https://www.google.com", std::get<0>(foundLinks.at(0)) );
  ASSERT_EQ( L"Google", std::get<1>(foundLinks.at(0)) );
  ASSERT_EQ( L"https://www.ibm.com", std::get<0>(foundLinks.at(1)) );
  ASSERT_EQ( L"Ibm", std::get<1>(foundLinks.at(1)) );
  ASSERT_EQ( L"https://www.opensuse.org", std::get<0>(foundLinks.at(2)) );
  ASSERT_EQ( L"OpenSuse", std::get<1>(foundLinks.at(2)) );
}

TEST(linkParser, collectNullLinks) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com"></a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

  _xmlDoc *htmlDocument = htmlReadDoc((const xmlChar *)htmldoc.c_str(), NULL, NULL,
                HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING);

  std::vector<std::tuple<std::wstring, std::wstring>> foundLinks {};
  ASSERT_TRUE (htmlDocument) << "The test should have been able to parse the html document";

  xmlNodePtr root_element = (xmlNodePtr)htmlDocument;
  collectLinks(root_element, foundLinks);
  ASSERT_THAT(3, foundLinks.size());
  ASSERT_EQ( L"https://www.ibm.com", std::get<0>(foundLinks.at(1)) );
  ASSERT_EQ( L"No Description", std::get<1>(foundLinks.at(1)) );
}
