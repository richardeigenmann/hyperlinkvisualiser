#include "stringConversion.h"

std::string wstringToString(const std::wstring ws)
{
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;

    return converter.to_bytes(ws);
}

std::wstring stringToWstring(const std::string s)
{
    using convert_type = std::codecvt_utf8<wchar_t>;
    std::wstring_convert<convert_type, wchar_t> converter;

    return converter.from_bytes(s);
}

std::wstring xmlCharToWideString(const xmlChar *xmlString)
{
    if (!xmlString) {
        return L"Null pointer passed to xmlCharToWideString";
    }
    try {
        std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> conv;
        return conv.from_bytes((const char *)xmlString);
    } catch (const std::range_error &e) {
        return L"wstring_convert failed";
    }
}
