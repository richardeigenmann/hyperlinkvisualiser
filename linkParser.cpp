#include "HyperLink.h"
#include "CurlWrapper.h"
#include "stringConversion.h"

bool downloadWebPage(HyperLink & hyperLink)
{
    CurlWrapper w;
    auto curlResult = w.getPage(wstringToString(hyperLink.href));

    if ( curlResult ) {
        hyperLink.webPage = curlResult.value();
        return true;
    } else {
        hyperLink.description = stringToWstring( w.errorMessage );
        return false;
    }
}

int collectLinks(xmlNode *a_node, std::vector<std::tuple<std::wstring, std::wstring>> & links)
{
    xmlNode *cur_node = NULL;
    int count = 0;
    for (cur_node = a_node; cur_node; cur_node = cur_node->next) {
        if (cur_node->type == XML_ELEMENT_NODE) {
            std::string name(reinterpret_cast<const char *>(cur_node->name));
            if (name == "a") {
                xmlChar *hrefProp = xmlGetProp(cur_node, (const xmlChar *)"href");
                std::wstring href = xmlCharToWideString(hrefProp);
                xmlFree(hrefProp);
                if (cur_node->children && cur_node->children->content) {
                  std::wstring content = xmlCharToWideString(cur_node->children->content);
                  links.emplace_back( std::make_tuple( href, content ) );
                } else {
                  links.emplace_back( std::make_tuple( href, L"No Description" ) );
                }
                ++count;
            }
        }
        count += collectLinks(cur_node->children, links);
    }
    return count;
}

std::vector<std::tuple<std::wstring, std::wstring>> parseLinks( HyperLink & hyperLink) {
    _xmlDoc *htmlDocument =
        htmlReadDoc((const xmlChar *)hyperLink.webPage.c_str(), NULL, NULL,
                    HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | HTML_PARSE_NOWARNING);

    std::vector<std::tuple<std::wstring, std::wstring>> foundLinks {};
    if (htmlDocument) {
        xmlNodePtr root_element = (xmlNodePtr)htmlDocument;
        collectLinks(root_element, foundLinks);
    }

    xmlFreeDoc(htmlDocument); // clear memory
    return foundLinks;
}

void populateChildren(HyperLink * hyperLink) {
    if ( downloadWebPage(*hyperLink) ) {
        auto links = parseLinks(*hyperLink);
        for ( auto link : links ) {
            std::wcout << "URL: " << std::get<0>(link)
              << '\n' << "Description: " << std::get<1>(link) << '\n';
            HyperLink h { std::get<0>(link), std::get<1>(link) };
            hyperLink->children.emplace_back(h);
        }
    }
}
