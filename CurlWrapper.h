#pragma once

#include <curl/curl.h>
#include <sstream>
#include <optional>
#include <iostream>


class CurlWrapper
{
public:
        /**
         * @brief This function will call CURL with the requested url and
         * returns a string of the data received.
         */
        std::optional<std::string> getPage(std::string pageUrl)
        {
                CURL *curl;
                curl = curl_easy_init();
                std::stringstream out;
                if (curl) {
                        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
                        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &out);
                        curl_easy_setopt(curl, CURLOPT_URL, pageUrl.c_str());
                        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
                        auto res = curl_easy_perform(curl);
                        curl_easy_cleanup(curl);
                        if ( res ) {
                                errorMessage = curl_easy_strerror(res);
                                std::cout << "Error: " << errorMessage << " (Code:"<< res
                                          << ") on retrieval of URL: " << pageUrl << std::endl;
                                return {};
                        }
                }
                return out.str();
        }

        std::string errorMessage {""};

private:
        /**
         * a helper function for CURL
         */
        static size_t write_data(void *ptr, size_t size, size_t nmemb, void *stream)
        {
                std::string data((const char *)ptr, (size_t)size * nmemb);
                *((std::stringstream *)stream) << data << std::endl;
                return size * nmemb;
        }

};
