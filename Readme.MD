# Graph from links

[ ![Codeship Status for richardeigenmann/hyperlinkvisualiser](https://app.codeship.com/projects/09a9f900-cf98-0135-7e2b-26b814663cce/status?branch=master)](https://app.codeship.com/projects/262253)
[![codecov](https://codecov.io/bb/richardeigenmann/hyperlinkvisualiser/branch/master/graph/badge.svg)](https://codecov.io/bb/richardeigenmann/hyperlinkvisualiser)

From Ale's stack of project cards:

Exercise:

Create a program that will create a graph or network from a series of links.

## Implementation

```bash
./graph URL
```

# Example:

```bash
./graph https://richardeigenmann.github.io/simplepage/links.htm
```

## Compiling

Note: I have done away with old compilers and outdated standards. This
code is for C++17 and will only compile on up-to-date compilers.

Dependencies:
libxml2
Boost

### Clone the repo:

```bash
cd <yourWorkDirectory>
git clone git@bitbucket.org:richardeigenmann/hyperlinkvisualiser.git
cd hyperlinkvisualiser
mkdir -p build
cd build
cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++ ..
#cmake -DCMAKE_CXX_COMPILER=/usr/bin/g++ ..
cmake --build . -- -j$(($(nproc) + 1))
```

### Using Docker

Compiling C++17 without rebuilding your computer can be a real issue.
We can use Docker to create a working C++17 environment that is isolated from
your workstation's environment.

I've stuck the pre-built container on Dockerhub and you can use it directly:

```bash
# checkout the git repo
cd <yourWorkDirectory>
git clone git@bitbucket.org:richardeigenmann/hyperlinkvisualiser.git
cd hyperlinkvisualiser
mkdir -p build

# run the container and mount the source directory into it
docker run -it -v <yourWorkDirectory>/hyperlinkvisualiser:/sourcedir -p 8080:8080 --rm --hostname cpp17container richardeigenmann/opensusecpp17 bash

# now you are inside the container
cd sourcedir/build
cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++ ..
#cmake -DCMAKE_CXX_COMPILER=/usr/bin/g++ ..
cmake --build . -- -j$(($(nproc) + 1))

./unit-tests
make graph_coverage
make doc

# run the program
./graph https://richardeigenmann.github.io/simplepage/links.htm

# to quit: Ctrl-c then
exit

# to resume the container take away the --rm in the run or 
# it is sort of removed then
docker ps -a # find the containter id
docker start -a -i <contrainerid>
```

### About the compiling container

[![](https://images.microbadger.com/badges/image/richardeigenmann/opensusecpp17.svg)](https://microbadger.com/images/richardeigenmann/opensusecpp17 "Get your own image badge on microbadger.com")
[![](https://images.microbadger.com/badges/version/richardeigenmann/opensusecpp17.svg)](https://microbadger.com/images/richardeigenmann/opensusecpp17 "Get your own version badge on microbadger.com")

It is a shocking 1.41 GB in size despite the recipe being relatively 
concise. See it in `Dockerfiles/OpenSuseCompiler/Dockerfile`
Since I like OpenSuSE and their rolling Tumbleweed distro in particular
the container is based on this image. It then installs Clang 5 and gcc 7 
and a couple of tools.

Unfortunately I chose to use the Hunter package manger for this project
which pulls down libcurl, libxml2 and boost from source and compiles
them. This makes builds last half an hour easily.

To speed things up I therefore added a dummy project to the container
build step called warmup. This causes Hunter to download and compile 
the desired libraries at container build time. When the container is 
subsequently used the heavy lifting has already been done and the 
actual compile is done in seconds.

To make the container more reusable it doesn't actually contain the 
project directory. This is instead mounted from the host filesystem
into the VOLUME `/sourcedir` with the -v parameter on the docker run 
command. The compiled executable is therefore also written to the host 
filesystem, But note that since it is not statically linked it can't run
there.


### The runtime container

To run the simple target binary we don't need 1.4 GB of compilers
and stuff! Alpine Linux comes in really small containers which are
of course quite bare. Unfortunately they use a wierd libc which 
doesn't work so well here but there is an alpine-glibc container
the fixes these issues.

The Dockerfile at `Dockerfiles/OpenSuseRunner/` uses the multi-stage
build facility of Docker. First it uses the `richardeigenmann/opensusecpp17`
image as a base and compiles the program.

Note that this fails if the `/sourcedir` Volume is used (mystery).

Note further that the COPY works locally to the source directory 
and we therefore have to copy the source directory to a subdirectoy
of `Dockerfiles/OpenSuseRunne/sourcedir` (annoying)

The Dockerfile then uses the multi-stage build facility by 
specifying a second `FROM` line in the recipe to pull the minimalist Alpine 
image with glibc. 

It then copies the binary, the the webdir and the C++ std 
library to the new container and gives it an `ENTRYPOINT`

The final image is about 38 MB in size.

```bash
# Copy sources
mkdir -p Dockerfiles/OpenSuseRunner/sourcedir/web
cp . Dockerfiles/OpenSuseRunner/sourcedir/
cp web/* Dockerfiles/OpenSuseRunner/sourcedir/web/

# build
docker build --rm=false -t opensuse/runner Dockerfiles/OpenSuseRunner/

# run 
docker run -t --hostname runner -p 8080:8080 --rm opensuse/runner

# kill
docker ps
docker kill <container>
```



## Code Coverage

```bash
make graph_coverage
google-chrome build/graph_coverage/index.html
```


## Visual Studio - TODO

```bash
cmake .. -G "Visual Studio 15 Win64"
```

## Webserver

I'm using the simple web server from Ole Christian Eidheim at
https://github.com/eidheim/Simple-Web-Server
Which is licensed under the MIT license.

## Json

I love Niels Lohmann's Json library. I lifted the file from here:
https://github.com/nlohmann/json
He has licensed it under the MIT license

## Hunter

This project attempts uses the Hunter packet manager for the dependencies.
TODO: Elaborate....

## D3 visualisation

see https://bl.ocks.org/mbostock/4339083
TODO: Elaborate....

## Documentation

```bash
mkdir -p build
cd build
cmake -DCMAKE_CXX_COMPILER=/usr/bin/clang++ ..
make doc
```


## TravisCI

We wanted to give Bitbucket a try and it turns out that TravisCI totally
depends on Github and doesn't work with Bitbucket.

## Codeship

The Codeship service uses the `codeship-services.yml` 
file to define the docker image to use for the build and
the `codeship-steps.yml` file to exeucte the build steps,
run the unit tests and upload the coverage report to 
Codecov.

