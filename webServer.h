#pragma once

#include "server_http.hpp"
#include <memory>
#include <fstream>
#include <experimental/filesystem> //C++17
//#include <filesystem> //C++17
#include <stdexcept>
#include "linkParser.h"
#include <set>
#include <optional>

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;
namespace fs = std::experimental::filesystem;
// namespace fs = std::filesystem;

class WebServer
{
public:
    WebServer(HyperLink *rootLink) : rootLink(rootLink)
    {
        server.config.port = 8080;
        std::cout << "Open your browser against: http://localhost:8080\n";
        server.resource["^/data$"]["GET"] =
            [rootLink,this](std::shared_ptr<HttpServer::Response> response,
                       std::shared_ptr<HttpServer::Request> request) {
                *response << streamNodesAsJson( rootLink );
            };

        // Handle the click on a URL /fetch/1
        server.resource["^/fetch/([0-9]+)$"]["GET"] =
            [rootLink,this](std::shared_ptr<HttpServer::Response> response,
                       std::shared_ptr<HttpServer::Request> request) {
                std::string number = request->path_match[1];

                std::cout << "Click node : " << number << std::endl;
                auto o = rootLink->findChild(std::stoi(number));
                if (o) {
                    std::cout << "We found the link!\n";
                    HyperLink * h = o.value();
                    std::cout << h->toString() << std::endl;
                    populateChildren(h);
                }

                *response << streamNodesAsJson( rootLink );
            };

            // Handle the click on a url which must retrieve a new url for the root node
            server.resource["^/url$"]["POST"] =
                [rootLink,this](std::shared_ptr<HttpServer::Response> response,
                           std::shared_ptr<HttpServer::Request> request) {
                    std::string newUrl = request->path_match[1];
                    std::cout << "New Url: " << newUrl << std::endl;
                    *response << streamNodesAsJson( rootLink );
                };

        server.default_resource["GET"] = [](std::shared_ptr<HttpServer::Response> response,
                                            std::shared_ptr<HttpServer::Request> request) {
            try {
                std::cout << "About to query thge filesystem for the fs::canonical(\"web\")\n";
                std::cout << "Current path is " << fs::current_path() << '\n';
                auto webserverRootPath = fs::canonical("web");
                std::cout << "webserverRootPath: " << webserverRootPath << '\n';
                auto requestSubPath = request->path;
                std::cout << "requestSubPath: " << requestSubPath << '\n';
                auto subPath = fs::canonical(webserverRootPath / requestSubPath);
                std::cout << "subPath: " << subPath << '\n';
                // Check if path is within web_root_path
                if (std::distance(webserverRootPath.begin(), webserverRootPath.end()) >
                        std::distance(subPath.begin(), subPath.end()) ||
                    !std::equal(webserverRootPath.begin(), webserverRootPath.end(),
                                subPath.begin()))
                    throw std::invalid_argument("path must be within root path");
                if (fs::is_directory(subPath))
                    subPath /= "index.html";

                SimpleWeb::CaseInsensitiveMultimap header;

                //    Uncomment the following line to enable Cache-Control
                //    header.emplace("Cache-Control", "max-age=86400");

                auto ifs = std::make_shared<std::ifstream>();
                ifs->open(subPath.string(), std::ifstream::in | std::ios::binary | std::ios::ate);

                if (*ifs) {
                    auto length = ifs->tellg();
                    ifs->seekg(0, std::ios::beg);

                    header.emplace("Content-Length", to_string(length));
                    response->write(header);

                    // Trick to define a recursive function within this scope (for example purposes)
                    class FileServer
                    {
                    public:
                        static void read_and_send(
                            const std::shared_ptr<HttpServer::Response> &response,
                            const std::shared_ptr<std::ifstream> &ifs)
                        {
                            // Read and send 128 KB at a time
                            static std::vector<char> buffer(
                                131072); // Safe when server is running on one thread
                            std::streamsize read_length;
                            if ((read_length = ifs->read(&buffer[0], static_cast<std::streamsize>(
                                                                         buffer.size()))
                                                   .gcount()) > 0) {
                                response->write(&buffer[0], read_length);
                                if (read_length == static_cast<std::streamsize>(buffer.size())) {
                                    response->send(
                                        [response, ifs](const SimpleWeb::error_code &ec) {
                                            if (!ec)
                                                read_and_send(response, ifs);
                                            else
                                                std::cerr << "Connection interrupted" << std::endl;
                                        });
                                }
                            }
                        }
                    };
                    FileServer::read_and_send(response, ifs);
                } else
                    throw std::invalid_argument("could not read file");
            } catch (const std::exception &e) {
                response->write(SimpleWeb::StatusCode::client_error_bad_request,
                                "Could not open path " + request->path + ": " + e.what());
            }
        };

        server.on_error = [](std::shared_ptr<HttpServer::Request> /*request*/,
                             const SimpleWeb::error_code & /*ec*/) {
            // Handle errors here
        };

        // server.start();
    }
    void start() { server.start(); }
    void stop() { server.stop(); }

    std::string streamNodesAsJson( const HyperLink *rootLink ) {
        std::stringstream stream;
        stream << rootLink->toJson();

        // Find length of content_stream (length received using content_stream.tellp())
        stream.seekp(0, std::ios::end);
        auto length = stream.tellp();

        std::stringstream stream2;
        stream2 << "HTTP/1.1 200 OK\r\nContent-Length: " << length << "\r\n\r\n"
            << stream.rdbuf();
        return stream2.str();
    }

private:
    HyperLink *rootLink;
    HttpServer server;
};
