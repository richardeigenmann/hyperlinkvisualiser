#include <gmock/gmock.h>
using namespace ::testing;

#include "webServer.h"
#include "HyperLink.h"
#include "linkParser.h"
#include "CurlWrapper.h"
#include <thread>
#include <chrono>


TEST(webServer, streamNodesAsJson) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:1234", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::string jsonText = webServer.streamNodesAsJson(&root);
    ASSERT_EQ("HTTP/1.1 200 OK\r\nContent-Length: 63\r\n\r\n" R"({"href":"http://localhost:1234","node":35,"text":"Static page"})", jsonText);
}


void startWebServer( WebServer * webServer ){
    webServer->start();
}

TEST(webServer, testGetData) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:1234", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/data");
    ASSERT_TRUE(page);
    ASSERT_EQ(R"({"href":"http://localhost:1234","node":36,"text":"Static page"})" "\n", page.value());
    webServer.stop();
    webServerThread.join();
}

TEST(webServer, testFetch0) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:8080", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/fetch/0");
    ASSERT_TRUE(page);
    ASSERT_EQ(R"({"href":"http://localhost:8080","node":37,"text":"Static page"})" "\n", page.value());
    webServer.stop();
    webServerThread.join();
}

TEST(webServer, testFetch1) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:8080", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/fetch/39");
    ASSERT_TRUE(page);
    ASSERT_EQ(R"({"href":"http://localhost:8080","node":38,"text":"Static page"})" "\n", page.value());
    webServer.stop();
    webServerThread.join();
}


TEST(webServer, testGetSlash) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:8080", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/");
    ASSERT_TRUE(page);
    ASSERT_LT(6000, page.value().length() ) << "Page content:\n" << page.value() << "\n";;
    webServer.stop();
    webServerThread.join();
}


TEST(webServer, testGetIndex) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:8080", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/index.html");
    ASSERT_TRUE(page);
    ASSERT_LT(6000, page.value().length() );
    webServer.stop();
    webServerThread.join();
}

// Interesting the webserver strips off the ../../ and gives us the root.
TEST(webServer, testGetBadPath) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:8080", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/../../index.html");
    ASSERT_TRUE(page);
    ASSERT_LT(6000, page.value().length() );
    webServer.stop();
    webServerThread.join();
}

//and it aso cleans out /sub/../../
TEST(webServer, testGetBadPathTraversal) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:8080", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/sub/../../index.html");
    ASSERT_TRUE(page);
    ASSERT_LT(6000, page.value().length() );
    webServer.stop();
    webServerThread.join();
}


TEST(webServer, testGetIncorrectFile) {
    std::string htmldoc = R"END(
<!DOCTYPE html>
<html lang="en-us">
  <head>
    <meta charset="utf-8">
    <title>Hello...</title>
  </head>
  <body>
    <h1>This is a page with links</h1>
    <a href="https://www.google.com">Google</a><br>
    <A href="https://www.ibm.com">Ibm</a><br>
    <a hREF="https://www.opensuse.org">OpenSuse</a>
  </body>
</html>
)END";

    HyperLink root { L"http://localhost:8080", L"Static page"};
    root.webPage = htmldoc;
    auto links = parseLinks(root);
    ASSERT_THAT(3, links.size());
    WebServer webServer{&root};
    std::thread webServerThread ( startWebServer, &webServer );
    CurlWrapper curl{};
    std::this_thread::sleep_for(std::chrono::milliseconds(800));
    auto page = curl.getPage("http://localhost:8080/NoSuchFile.html");
    ASSERT_TRUE(page);
    ASSERT_THAT(page.value(), StartsWith( R"(Could not open path /NoSuchFile.html: filesystem error: cannot canonicalize: No such file or directory)" ) );
    webServer.stop();
    webServerThread.join();
}
