#include <iostream>
#include "HyperLink.h"
#include "webServer.h"
#include "linkParser.h"

void retrieveAndRunWebserver( const std::string & url ) {
  HyperLink rootLink = HyperLink{stringToWstring(url), L"Root Node"};
  populateChildren(&rootLink);
  WebServer w{&rootLink};
  w.start();
}

/**
 * Pulls the supplied web page and outputs the links it finds on the page
 */
int main(int argc, char *argv[])
{
    if (argc != 2) {
        std::cout << "Usage: graph URL\n";
        return 1;
    }
    std::string url = argv[1];

    retrieveAndRunWebserver( url );

    return 0;
}
