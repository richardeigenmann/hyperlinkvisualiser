#include <gmock/gmock.h>
using namespace ::testing;

#include "HyperLink.h"

#include <nlohmann/json.hpp>
using json = nlohmann::json;

TEST(HyperLink, constructor)
{
    HyperLink hyperLink{L"https://www.example.com", L"MyDescription"};
    ASSERT_EQ(L"https://www.example.com", hyperLink.href);
    ASSERT_EQ(L"MyDescription", hyperLink.description);
}

TEST(HyperLink, toString)
{
    HyperLink hyperLink {L"https://www.example.com", L"MyDescription"};
    std::string node = std::to_string(hyperLink.node);
    std::string toString = hyperLink.toString();
    ASSERT_EQ("Link: " + node + "\nUrl: https://www.example.com\nDescription: MyDescription\nwebPage.size(): "
              "0\nChildren: 0\n",
              hyperLink.toString());
}

TEST(HyperLink, toJson)
{
    HyperLink hyperLink{L"https://www.example.com", L"MyDescription"};
    std::string node = std::to_string(hyperLink.node);
    json myJson = hyperLink.toJson();
    std::string myString = myJson.dump();
    ASSERT_EQ("{\"href\":\"https://www.example.com\",\"node\":" + node + ",\"text\":\"MyDescription\"}",
              myString);
}


TEST(HyperLink, toJsonTree)
{
    HyperLink root{L"https://www.example.com", L"MyDescription"};
    std::string rootNode = std::to_string(root.node);
    HyperLink child1{L"https://www.child1.com", L"Child 1"};
    std::string child1Node = std::to_string(child1.node);
    HyperLink child2{L"https://www.child2.com", L"Child 2"};
    std::string child2Node = std::to_string(child2.node);
    HyperLink child3{L"https://www.child3.com", L"Child 3"};
    std::string child3Node = std::to_string(child3.node);
    HyperLink child21{L"https://www.child21.com", L"Child 21"};
    std::string child21Node = std::to_string(child21.node);
    root.children.push_back(child1);
    child2.children.push_back(child21);
    root.children.push_back(child2);
    root.children.push_back(child3);

    json myJson = root.toJson();
    std::string myString = myJson.dump();
    ASSERT_EQ("{\"children\":[{\"href\":\"https://www.child1.com\",\"node\":" + child1Node + ",\"text\":\"Child "
              "1\"},{\"children\":[{\"href\":\"https://www.child21.com\",\"node\":" + child21Node +
              ",\"text\":\"Child 21\"}],\"href\":\"https://www.child2.com\",\"node\":" + child2Node + ",\"text\":\"Child "
              "2\"},{\"href\":\"https://www.child3.com\",\"node\":" + child3Node + ",\"text\":\"Child "
              "3\"}],\"href\":\"https://www.example.com\",\"node\":" + rootNode + ",\"text\":\"MyDescription\"}",
              myString);
}

TEST(HyperLink, findChildRoot)
{
    HyperLink root{L"https://www.example.com", L"MyDescription"};
    HyperLink child1{L"https://www.child1.com", L"Child 1"};
    HyperLink child2{L"https://www.child2.com", L"Child 2"};
    HyperLink child3{L"https://www.child3.com", L"Child 3"};
    HyperLink child21{L"https://www.child21.com", L"Child 21"};
    root.children.push_back(child1);
    child2.children.push_back(child21);
    root.children.push_back(child2);
    root.children.push_back(child3);

    std::optional<HyperLink *> foundChild = root.findChild(root.node);
    ASSERT_TRUE(foundChild);
    HyperLink *foundHyperLinkPtr = foundChild.value();
    ASSERT_EQ(&root, foundHyperLinkPtr);
}

TEST(HyperLink, findChildFirstLevel)
{
    HyperLink root{L"https://www.example.com", L"MyDescription"};
    HyperLink child1{L"https://www.child1.com", L"Child 1"};
    HyperLink child2{L"https://www.child2.com", L"Child 2"};
    HyperLink child3{L"https://www.child3.com", L"Child 3"};
    HyperLink child21{L"https://www.child21.com", L"Child 21"};
    root.children.push_back(child1);
    child2.children.push_back(child21);
    root.children.push_back(child2);
    root.children.push_back(child3);

    std::optional<HyperLink *> foundChild1 = root.findChild(child1.node);
    ASSERT_TRUE(foundChild1);
    HyperLink *foundHyperLinkPtr1 = foundChild1.value();
    ASSERT_EQ(child1.node, foundHyperLinkPtr1->node);

    std::optional<HyperLink *> foundChild2 = root.findChild(child2.node);
    ASSERT_TRUE(foundChild2);
    HyperLink *foundHyperLinkPtr2 = foundChild2.value();
    ASSERT_EQ(child2.node, foundHyperLinkPtr2->node);

    std::optional<HyperLink *> foundChild3 = root.findChild(child3.node);
    ASSERT_TRUE(foundChild3);
    HyperLink *foundHyperLinkPtr3 = foundChild3.value();
    ASSERT_EQ(child3.node, foundHyperLinkPtr3->node);
}

TEST(HyperLink, findChildSecondLevel)
{
    HyperLink root{L"https://www.example.com", L"MyDescription"};
    HyperLink child1{L"https://www.child1.com", L"Child 1"};
    HyperLink child2{L"https://www.child2.com", L"Child 2"};
    HyperLink child3{L"https://www.child3.com", L"Child 3"};
    HyperLink child21{L"https://www.child21.com", L"Child 21"};
    root.children.push_back(child1);
    // sequence is important because once we push_back child2 the object in the
    // vector is no longer the object child2 but a copy. A subsequent modification
    // like adding a child will not be reflected.
    child2.children.push_back(child21);
    root.children.push_back(child2);
    root.children.push_back(child3);

    std::optional<HyperLink *> foundChild = root.findChild(child21.node);
    ASSERT_TRUE(foundChild);
    HyperLink *foundHyperLinkPtr = foundChild.value();
    ASSERT_EQ(child21.node, foundHyperLinkPtr->node);
}

TEST(HyperLink, findMissingChild)
{
    HyperLink root{L"https://www.example.com", L"MyDescription"};
    HyperLink child1{L"https://www.child1.com", L"Child 1"};
    HyperLink child2{L"https://www.child2.com", L"Child 2"};
    HyperLink child3{L"https://www.child3.com", L"Child 3"};
    HyperLink child21{L"https://www.child21.com", L"Child 21"};
    root.children.push_back(child1);
    root.children.push_back(child2);
    root.children.push_back(child3);
    child2.children.push_back(child21);

    std::optional<HyperLink *> foundChild = root.findChild(0);
    ASSERT_FALSE(foundChild);
}
