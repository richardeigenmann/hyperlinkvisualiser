#pragma once

#include <iostream>
#include <vector>
#include <nlohmann/json.hpp>
using json = nlohmann::json;

#include "stringConversion.h"




/**
 * @brief A class representing a link and a vector of children
 */
class HyperLink
{
public:
    /**
     * @brief Constructs a new link with the url, and the description.
     * @href the URL of the Hyperlink
     * @description The description of the Hyperlink
     */
    HyperLink(std::wstring href, std::wstring description)
        : href(href), description(description), node(counter++) {};

    /**
     * @brief an internal node number
     */
    int node;

    /**
     * @brief the child pages
     *
     * Is this a good way to hold the children? The push_back copies the
     * objects.
     */
    std::vector<HyperLink> children{};

    /**
     * @brief returns a json representation of the node with href, description and node number
     */
    json toJson() const
    {
        json j = {{"href", wstringToString(href)},
                  {"text", wstringToString(description)},
                  {"node", node}};
        for (auto & child : children) {
            j["children"].push_back( child.toJson() );
        }
        return j;
    }

    /**
     * @brief Returns a description of the link
     * @return The description of the link
     */
    std::string toString() const
    {
        std::stringstream ss;
        ss << "Link: " << node << std::endl
           << "Url: " << wstringToString(href) << std::endl
           << "Description: " << wstringToString(description) << std::endl
           << "webPage.size(): " << webPage.size() << std::endl
           << "Children: " << children.size() << std::endl;
        return ss.str();
    }

    /**
     * @brief the URL or the link
     */
    std::wstring href;

    /**
     * @brief A description for the Link
     */
    std::wstring description;

    /**
     * @brief the data on the webpage
     */
    std::string webPage{""};

    /**
     * @brief Returns an optional with a pointer to the HyperLink
     * corresponding to the searchNumber
     * @param searchNumber the number to search for
     * @return an optional<Link> with the link found
     */
    std::optional<HyperLink *> findChild(int searchNumber)
    {
        if (node == searchNumber) {
            return this;
        } else {
            for (HyperLink & c : children) {
                auto o = c.findChild(searchNumber);
                if (o) {
                    return o;
                }
            }
        }
        return {};
    }

private:

    /**
     * @brief static variable to help create the next id
     */
    static int counter;
};
