#pragma once

#include <locale>
#include <codecvt>
#include <libxml/HTMLparser.h>


/**
 * @brief Converts a std::wstring to a std::string
 * @param ws the std::wstring to convert
 * @return the std::string
 * @see
 * https://stackoverflow.com/questions/4804298/how-to-convert-wstring-into-string
 */
std::string wstringToString(const std::wstring ws);

/**
 * @brief Converts a std::string to a std::wstring
 *
 * @param s the std::string to convert
 * @return the std::wstring
 * @see
 * https://stackoverflow.com/questions/4804298/how-to-convert-wstring-into-string
 */
std::wstring stringToWstring(const std::string s);

/**
 * @see
 * https://stackoverflow.com/questions/14107268/libxml2-xmlchar-to-stdwstring
 */
std::wstring xmlCharToWideString(const xmlChar *xmlString);
